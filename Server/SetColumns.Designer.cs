﻿namespace Server
{
    partial class SetColumns
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SetColumns));
            this.groupPanel1 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.listold = new System.Windows.Forms.ListBox();
            this.groupPanel2 = new DevComponents.DotNetBar.Controls.GroupPanel();
            this.listnew = new System.Windows.Forms.ListBox();
            this.qu = new DevComponents.DotNetBar.ButtonX();
            this.quall = new DevComponents.DotNetBar.ButtonX();
            this.lai = new DevComponents.DotNetBar.ButtonX();
            this.laiall = new DevComponents.DotNetBar.ButtonX();
            this.buttonX1 = new DevComponents.DotNetBar.ButtonX();
            this.groupPanel1.SuspendLayout();
            this.groupPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupPanel1
            // 
            this.groupPanel1.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel1.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel1.Controls.Add(this.listold);
            this.groupPanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.groupPanel1.Location = new System.Drawing.Point(0, 0);
            this.groupPanel1.Name = "groupPanel1";
            this.groupPanel1.Size = new System.Drawing.Size(130, 288);
            // 
            // 
            // 
            this.groupPanel1.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel1.Style.BackColorGradientAngle = 90;
            this.groupPanel1.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel1.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderBottomWidth = 1;
            this.groupPanel1.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel1.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderLeftWidth = 1;
            this.groupPanel1.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderRightWidth = 1;
            this.groupPanel1.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel1.Style.BorderTopWidth = 1;
            this.groupPanel1.Style.CornerDiameter = 4;
            this.groupPanel1.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel1.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel1.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel1.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            this.groupPanel1.TabIndex = 0;
            this.groupPanel1.Text = "现有数据";
            // 
            // listold
            // 
            this.listold.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listold.FormattingEnabled = true;
            this.listold.ItemHeight = 12;
            this.listold.Location = new System.Drawing.Point(0, 0);
            this.listold.Name = "listold";
            this.listold.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listold.Size = new System.Drawing.Size(124, 264);
            this.listold.TabIndex = 1;
            // 
            // groupPanel2
            // 
            this.groupPanel2.CanvasColor = System.Drawing.SystemColors.Control;
            this.groupPanel2.ColorSchemeStyle = DevComponents.DotNetBar.eDotNetBarStyle.Office2007;
            this.groupPanel2.Controls.Add(this.listnew);
            this.groupPanel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.groupPanel2.Location = new System.Drawing.Point(218, 0);
            this.groupPanel2.Name = "groupPanel2";
            this.groupPanel2.Size = new System.Drawing.Size(132, 288);
            // 
            // 
            // 
            this.groupPanel2.Style.BackColor2SchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground2;
            this.groupPanel2.Style.BackColorGradientAngle = 90;
            this.groupPanel2.Style.BackColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBackground;
            this.groupPanel2.Style.BorderBottom = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderBottomWidth = 1;
            this.groupPanel2.Style.BorderColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelBorder;
            this.groupPanel2.Style.BorderLeft = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderLeftWidth = 1;
            this.groupPanel2.Style.BorderRight = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderRightWidth = 1;
            this.groupPanel2.Style.BorderTop = DevComponents.DotNetBar.eStyleBorderType.Solid;
            this.groupPanel2.Style.BorderTopWidth = 1;
            this.groupPanel2.Style.CornerDiameter = 4;
            this.groupPanel2.Style.CornerType = DevComponents.DotNetBar.eCornerType.Rounded;
            this.groupPanel2.Style.TextAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Center;
            this.groupPanel2.Style.TextColorSchemePart = DevComponents.DotNetBar.eColorSchemePart.PanelText;
            this.groupPanel2.Style.TextLineAlignment = DevComponents.DotNetBar.eStyleTextAlignment.Near;
            this.groupPanel2.TabIndex = 1;
            this.groupPanel2.Text = "选择数据";
            // 
            // listnew
            // 
            this.listnew.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listnew.FormattingEnabled = true;
            this.listnew.ItemHeight = 12;
            this.listnew.Location = new System.Drawing.Point(0, 0);
            this.listnew.Name = "listnew";
            this.listnew.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.listnew.Size = new System.Drawing.Size(126, 264);
            this.listnew.TabIndex = 0;
            // 
            // qu
            // 
            this.qu.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.qu.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.qu.Location = new System.Drawing.Point(148, 21);
            this.qu.Name = "qu";
            this.qu.Size = new System.Drawing.Size(50, 42);
            this.qu.TabIndex = 2;
            this.qu.Text = "<font color=\"#758C48\" size=\"16\">&gt;</font>";
            this.qu.Click += new System.EventHandler(this.qu_Click);
            // 
            // quall
            // 
            this.quall.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.quall.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.quall.Location = new System.Drawing.Point(148, 79);
            this.quall.Name = "quall";
            this.quall.Size = new System.Drawing.Size(50, 42);
            this.quall.TabIndex = 3;
            this.quall.Text = "<font color=\"#758C48\" size=\"16\">&gt;&gt;</font>";
            this.quall.Click += new System.EventHandler(this.quall_Click);
            // 
            // lai
            // 
            this.lai.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.lai.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.lai.Location = new System.Drawing.Point(148, 137);
            this.lai.Name = "lai";
            this.lai.Size = new System.Drawing.Size(50, 42);
            this.lai.TabIndex = 4;
            this.lai.Text = "<font color=\"#758C48\" size=\"16\">\r\n&lt;\r\n</font>";
            this.lai.Click += new System.EventHandler(this.lai_Click);
            // 
            // laiall
            // 
            this.laiall.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.laiall.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.laiall.Location = new System.Drawing.Point(148, 195);
            this.laiall.Name = "laiall";
            this.laiall.Size = new System.Drawing.Size(50, 42);
            this.laiall.TabIndex = 5;
            this.laiall.Text = "<font color=\"#758C48\" size=\"16\">&lt;&lt;</font>";
            this.laiall.Click += new System.EventHandler(this.laiall_Click);
            // 
            // buttonX1
            // 
            this.buttonX1.AccessibleRole = System.Windows.Forms.AccessibleRole.PushButton;
            this.buttonX1.ColorTable = DevComponents.DotNetBar.eButtonColor.OrangeWithBackground;
            this.buttonX1.Location = new System.Drawing.Point(148, 253);
            this.buttonX1.Name = "buttonX1";
            this.buttonX1.Size = new System.Drawing.Size(50, 32);
            this.buttonX1.TabIndex = 6;
            this.buttonX1.Text = "保存";
            this.buttonX1.Click += new System.EventHandler(this.buttonX1_Click);
            // 
            // SetColumns
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(350, 288);
            this.Controls.Add(this.buttonX1);
            this.Controls.Add(this.laiall);
            this.Controls.Add(this.lai);
            this.Controls.Add(this.quall);
            this.Controls.Add(this.qu);
            this.Controls.Add(this.groupPanel2);
            this.Controls.Add(this.groupPanel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "SetColumns";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "选择导出数据";
            this.Load += new System.EventHandler(this.SetColumns_Load);
            this.groupPanel1.ResumeLayout(false);
            this.groupPanel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel1;
        private DevComponents.DotNetBar.Controls.GroupPanel groupPanel2;
        private DevComponents.DotNetBar.ButtonX qu;
        private System.Windows.Forms.ListBox listold;
        private System.Windows.Forms.ListBox listnew;
        private DevComponents.DotNetBar.ButtonX quall;
        private DevComponents.DotNetBar.ButtonX lai;
        private DevComponents.DotNetBar.ButtonX laiall;
        private DevComponents.DotNetBar.ButtonX buttonX1;
    }
}